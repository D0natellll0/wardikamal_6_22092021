// import Profil, { printProfil } from "./data.js";

// const profil = new Profil ("Mimi Keel", "243", "London"/*, "UK", "Voir le beau dans le quotidien", 400, "MimiKeel.jpg"*/);

// printProfil(profil);

// //Pour faire appel à des propriété d'un objet avec le destructuring
// const personOne = {
//   name : 'John',
//   age: 24,
//   address: {
//     city: 'ici',
//     state:'peu'
//   }
// }

// function printUser ({name, age}) /*les accolades ont surement un role important pour faire une référence aux propriétés de l'objet qui est passé en référence à l'appel de la fonction. */{
//   console.log(`Name is: ${name}. Age is ${age}`)
// }

// printUser(personOne)/* on fait référence à l'objet */
